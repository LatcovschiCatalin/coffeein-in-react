import * as React from "react";
import { useState, useEffect } from "react";
import { ThemeProvider } from "@emotion/react";
import { createTheme, Box } from "@mui/material";

const theme = createTheme({});

const Top = () => {
  const [showTopBtn, setShowTopBtn] = useState(false);
  useEffect(() => {
    window.addEventListener("scroll", () => {
      if (window.scrollY > window.innerHeight) {
        setShowTopBtn(true);
      } else {
        setShowTopBtn(false);
      }
    });
  }, []);
  const goToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };
  return (
    <React.Fragment>
      <ThemeProvider theme={theme}>
        {showTopBtn && (
          <Box
            component="img"
            src="/images/left-arrow.png"
            sx={{
              cursor: "pointer",
              position: "fixed",
              bottom: "80px",
              left: "30px",
              width: "18px",
              zIndex: 1000,
              bgcolor: "#303030",
              boxShadow: "0 0 8px 0 #000",
              transform: "rotate(90deg)",
              p: "12px",
              borderRadius: "50%",
            }}
            onClick={goToTop}
          >
            {/* {this.state.show} */}
          </Box>
        )}
      </ThemeProvider>
    </React.Fragment>
  );
};

export default Top;
