import * as React from "react";
import { Component } from "react";
import { Link } from "react-router-dom";
import { createTheme } from "@mui/material/styles";
import { ThemeProvider, Box } from "@mui/material";

const theme = createTheme({
  palette: {
    text: {
      primary: "#E5E5E5",
      secondary: "#c4c2c2",
    },
  },
});

interface NavbarProps {
  logo: string;
  phone: string;
  menu: string;
}

interface NavbarState {}

class Navbar extends React.Component<NavbarProps, NavbarState> {
  constructor(props: NavbarProps) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <ThemeProvider theme={theme}>
        <Box
          sx={{
            bgcolor: "#1a1a1a",
            px: "100px",
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            fontSize: "22px",
            height: "70px",
          }}
        >
          <Link to="/">
            <Box
              component="img"
              src={this.props.logo}
              sx={{
                maxHeight: "50px",
              }}
            ></Box>
          </Link>
          <Box>{this.props.menu}</Box>
          <Box component="a" href={"tel:" + this.props.phone}>
            {this.props.phone}
          </Box>
        </Box>
        <Box
          sx={{
            height: "10px",
            bgcolor: "#60514a",
          }}
        ></Box>
      </ThemeProvider>
    );
  }
}

export default Navbar;
