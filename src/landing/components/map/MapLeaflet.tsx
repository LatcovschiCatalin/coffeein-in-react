import * as React from "react";
import { Component } from "react";
import { render } from "react-dom";

import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import L from "leaflet";
interface MapLeafletProps {
  coordinates: string;
}

interface MapLeafletState {}

class MapLeaflet extends React.Component<MapLeafletProps, MapLeafletState> {
  constructor(props: MapLeafletProps) {
    super(props);
    this.state = {};
  }

  render() {
    let map_data = this.props.coordinates.split(",");
    const position = { lat: Number(map_data[1]), lng: Number(map_data[2]) };
    return (
      <MapContainer
        style={{
          width: "600px",
          height: "309px",
          borderRadius: "12px",
          marginTop: "20px",
        }}
        center={position}
        zoom={10}
        scrollWheelZoom={true}
      >
        <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
        <Marker position={position}>
          <Popup>{map_data[0]}</Popup>
        </Marker>
      </MapContainer>
    );
  }
}

export default MapLeaflet;
