import { ThemeProvider } from "@emotion/react";
import { createTheme, Box, Button } from "@mui/material";
import * as React from "react";
import { Component, SyntheticEvent } from "react";
import data from "../../../languages/languages.json";
import Footer from "../../components/footer/Footer";
import Navbar from "../../components/navbar/Navbar";
import { getCookie } from "../../../cookieAccess";
import { Link } from "react-router-dom";
const theme = createTheme({
  palette: {
    text: {
      primary: "#E5E5E5",
      secondary: "#c4c2c2",
    },
  },
});
interface ProductsProps {}

interface ProductsState {
  items: [
    {
      id: string;
      logo: {
        path: string;
      };
      mainProductImage: {
        path: string;
      };
      name: string;
      phone: string;
      slug: string;
      mail: string;
      coordinates: string;
      color: string;
      slogan: [
        {
          shortName: string;
          content: string;
        }
      ];
      isMainCompany: boolean;
      instagram: string;
      facebook: string;
      youtube: string;
    }
  ];
  products: [
    {
      category: {
        id: string;
        name: [
          {
            shortName: string;
            content: string;
          },
          {
            shortName: string;
            content: string;
          },
          {
            shortName: string;
            content: string;
          }
        ];
        slug: string;
      };
      products: [
        {
          id: string;
          image: {
            path: string;
          };
          title: [
            {
              shortName: string;
              content: string;
            }
          ];
          description: [
            {
              shortName: string;
              content: string;
            }
          ];
          mainDescription: [
            {
              shortName: string;
              content: string;
            }
          ];
        }
      ];
    }
  ];
  category: string;
  searchTerm: string;
  company: string;
  DataisLoaded: boolean;
}

class Products extends React.Component<ProductsProps, ProductsState> {
  url = "http://localhost:3000/api/";

  constructor(props: ProductsProps) {
    super(props);
    this.state = {
      items: [
        {
          id: "",
          logo: {
            path: "",
          },
          mainProductImage: {
            path: "",
          },
          name: "",
          phone: "",
          slug: "",
          mail: "",
          coordinates: "",
          color: "",
          slogan: [
            {
              shortName: "",
              content: "",
            },
          ],
          isMainCompany: false,
          instagram: "",
          facebook: "",
          youtube: "",
        },
      ],
      products: [
        {
          category: {
            id: "",
            name: [
              {
                shortName: "",
                content: "",
              },
              {
                shortName: "",
                content: "",
              },
              {
                shortName: "",
                content: "",
              },
            ],
            slug: "",
          },
          products: [
            {
              id: "",
              image: {
                path: "",
              },
              title: [
                {
                  shortName: "",
                  content: "",
                },
              ],
              description: [
                {
                  shortName: "",
                  content: "",
                },
              ],
              mainDescription: [
                {
                  shortName: "",
                  content: "",
                },
              ],
            },
          ],
        },
      ],
      category: "all",
      searchTerm: "",
      company: "",
      DataisLoaded: false,
    };
  }
  componentDidMount() {
    fetch(`${this.url}companies`)
      .then((res) => res.json())
      .then((json) => {
        this.setState({
          items: json.docs,
          DataisLoaded: true,
        });
      });
    fetch(
      `${this.url}products/sorted?page=&searchTerm=&productCategory=all&company=coffeein`
    )
      .then((res) => res.json())
      .then((json) => {
        this.setState({
          products: json.products,
        });
      });
  }

  setCategory = (e: React.SyntheticEvent<HTMLButtonElement>): void => {
    let i = this.languageIndex();
    this.setState({
      category:
        this.state.products.filter(
          //@ts-ignore
          (el) => el.category.name[i].content === e.target.textContent
        ).length === 1
          ? this.state.products.filter(
              //@ts-ignore
              (el) => el.category.name[i].content === e.target.textContent
            )[0].category.slug
          : "all",
    });
  };
  languageIndex() {
    let i = 0;
    let languages = this.state.items[0].slogan.map((el) =>
      el.shortName.toUpperCase()
    );
    let lang = getCookie("lang") ? getCookie("lang").toUpperCase() : "RO";
    for (let j = 0; j < languages.length; j++) {
      if (lang === languages[j]) {
        i = j;
      }
    }
    return i;
  }
  render() {
    let i = this.languageIndex();
    let slug =
      window.location.href.split("/")[
        window.location.href.split("/").length - 1
      ];
    let company = this.state.items.filter((el) => el.slug === slug);

    if (!this.state.DataisLoaded) {
      return (
        <div>
          <h1> Pleses wait some time.... </h1>{" "}
        </div>
      );
    }

    return (
      <React.Fragment>
        <Navbar
          logo={this.url + company[0].logo.path}
          phone={company[0].phone}
          menu={data[i].MENU}
        />
        <ThemeProvider theme={theme}>
          <Box
            sx={{
              px: "100px",
            }}
          >
            <Box
              sx={{
                mt: "40px",
              }}
            >
              <Link to="/">
                <Button
                  variant="outlined"
                  href="#outlined-buttons"
                  style={{
                    backgroundColor: "#60514a",
                    color: "#eae9e9",
                    fontSize: "16px",
                    border: "none",
                    filter: "drop-shadow(4px 8px 6px rgba(0, 0, 0, 0.5))",
                    padding: "5px 10px",
                    borderRadius: "8px",
                    width: "100px",
                    textTransform: "none",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "flex-start",
                  }}
                >
                  <Box
                    component="img"
                    src="/images/left-arrow.png"
                    sx={{
                      pr: "10px",
                      width: "16px",
                    }}
                  ></Box>
                  <Box component="span">{data[i].BACK}</Box>
                </Button>
              </Link>
            </Box>

            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  pb: "20px",
                  alignItems: "center",
                  mt: "30px",
                }}
              >
                <Box key="all">
                  <Button
                    onClick={this.setCategory}
                    className="all"
                    variant="contained"
                    sx={{
                      bgcolor:
                        this.state.category === "all"
                          ? company[0].color
                          : "#303030",
                      border:
                        this.state.category === "all"
                          ? ""
                          : "2px solid #e5e5e5;",
                      color: "#eae9e9",
                      borderRadius: "8px",
                      minWidth: "100px",
                      mr: "30px",
                      filter: "drop-shadow(4px 8px 6px rgba(0, 0, 0, 0.5))",
                      "&:hover": {
                        bgcolor:
                          this.state.category === "all"
                            ? company[0].color
                            : "#303030",
                      },
                      py: "7px",
                      px: "10px",
                      textTransform: "none",
                    }}
                  >
                    {data[i].ALL}
                  </Button>
                </Box>

                {this.state.products.map((el) => (
                  <Box key={el.category.id}>
                    <Button
                      onClick={this.setCategory}
                      variant="contained"
                      sx={{
                        bgcolor:
                          this.state.category === el.category.slug
                            ? company[0].color
                            : "#303030",
                        border:
                          this.state.category === el.category.slug
                            ? ""
                            : "2px solid #e5e5e5;",
                        color: "#eae9e9",
                        borderRadius: "8px",
                        minWidth: "100px",
                        mr: "30px",
                        filter: "drop-shadow(4px 8px 6px rgba(0, 0, 0, 0.5))",
                        "&:hover": {
                          bgcolor:
                            this.state.category === el.category.slug
                              ? company[0].color
                              : "#303030",
                        },
                        py: "7px",
                        px: "10px",
                        textTransform: "none",
                      }}
                    >
                      {el.category.name[i].content}
                    </Button>
                  </Box>
                ))}
              </Box>
              <Box>{this.state.category}</Box>
            </Box>
          </Box>
        </ThemeProvider>
        <Footer
          logo={this.url + company[0].logo.path}
          phone={company[0].phone}
          mail={company[0].mail}
          instagram={company[0].instagram}
          youtube={company[0].youtube}
          facebook={company[0].facebook}
          coordinates={company[0].coordinates}
        />
      </React.Fragment>
    );
  }
}

export default Products;
