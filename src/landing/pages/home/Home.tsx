import { ThemeProvider } from "@emotion/react";
import { createTheme, Box } from "@mui/material";
import * as React from "react";
import { Component } from "react";
import { Cookies } from "react-cookie";
import Menu from "./components/menu/Menu";
import Slick from "./components/slider-info/Slick";

const theme = createTheme({
  palette: {
    text: {
      primary: "#E5E5E5",
      secondary: "#c4c2c2",
    },
  },
});
interface HomeProps {}

interface HomeState {}

class Home extends React.Component<HomeProps, HomeState> {
  constructor(props: HomeProps) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <React.Fragment>
        <ThemeProvider theme={theme}>
          <Box
            sx={{
              px:"100px",
              pt:"50px",
              pb:"100px"
            }}
          >
            <Menu />
            <Slick />
          </Box>
        </ThemeProvider>
      </React.Fragment>
    );
  }
}

export default Home;
