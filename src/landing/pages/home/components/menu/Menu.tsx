import * as React from "react";
import { Component } from "react";
import data from "../../../../../languages/languages.json";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import { createTheme } from "@mui/material/styles";
import { ThemeProvider, Box } from "@mui/material";
import { getCookie, setCookie } from "../../../../../cookieAccess";
import { Link } from "react-router-dom";
const theme = createTheme({
  palette: {
    text: {
      primary: "#E5E5E5",
      secondary: "#c4c2c2",
    },
  },
});

interface MenuProps {}

interface MenuState {
  items: [
    {
      id: string;
      logo: {
        path: string;
      };
      mainProductImage: {
        path: string;
      };
      name: string;
      phone: string;
      slug: string;
      mail: string;
      coordinates: string;
      color: string;
      slogan: [
        {
          shortName: string;
          content: string;
        }
      ];
      isMainCompany: boolean;
      instagram: string;
      facebook: string;
      youtube: string;
    }
  ];
  DataisLoaded: boolean;
}

class Menu extends React.Component<MenuProps, MenuState> {
  url = "http://localhost:3000/api/";
  handleChange(event: SelectChangeEvent) {
    event.preventDefault();
    setCookie("lang", event.target.value.toLowerCase());
    window.location.reload();
  }
  constructor(props: MenuProps) {
    super(props);

    this.state = {
      items: [
        {
          id: "",
          logo: {
            path: "",
          },
          mainProductImage: {
            path: "",
          },
          name: "",
          phone: "",
          slug: "",
          mail: "",
          coordinates: "",
          color: "",
          slogan: [
            {
              shortName: "",
              content: "",
            },
          ],
          isMainCompany: false,
          instagram: "",
          facebook: "",
          youtube: "",
        },
      ],
      DataisLoaded: false,
    };
  }

  componentDidMount() {
    fetch(`${this.url}companies`)
      .then((res) => res.json())
      .then((json) => {
        this.setState({
          items: json.docs,
          DataisLoaded: true,
        });
      });
  }

  render() {
    let nr_products = 1,
      i = 0;
    let main = this.state.items.filter((el) => el.isMainCompany);
    let languages = this.state.items[0].slogan.map((el) =>
      el.shortName.toUpperCase()
    );
    let lang = getCookie("lang") ? getCookie("lang").toUpperCase() : "RO";
    for (let j = 0; j < languages.length; j++) {
      if (lang === languages[j]) {
        i = j;
      }
    }

    if (!this.state.DataisLoaded) {
      return (
        <div>
          <h1> Pleses wait some time.... </h1>{" "}
        </div>
      );
    } else {
      return (
        <React.Fragment>
          <ThemeProvider theme={theme}>
            <Box
              sx={{
                p: "80px",
                borderRadius: "30px",
                bgcolor: "#1a1a1a",
                height: "auto",
                width: "auto",
                backgroundImage: "url('/images/bg1.png')",
                backgroundRepeat: "no-repeat",
                backgroundSize: "102%",
                filter: "drop-shadow(0 30px 10px #000000)",
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  width: "100%",
                  justifyContent: "space-between",
                }}
              >
                <Box sx={{ display: "flex", alignItems: "center" }}>
                  {this.state.items.map((item) => (
                    <Link to={"/products/" + item.slug}>
                      <Box
                        component="img"
                        src={this.url + item.logo.path}
                        sx={{ pr: "50px" }}
                        key={item.id}
                      ></Box>
                    </Link>
                  ))}
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <Box
                    component="a"
                    href={"tel:" + main[0].phone}
                    sx={{ fontSize: "24px" }}
                  >
                    {main[0].phone}
                  </Box>
                  <FormControl>
                    <Select
                      MenuProps={{
                        disableScrollLock: true,
                        disableAutoFocusItem: true,
                      }}
                      sx={{
                        color: "text.secondary",
                        cursor: "pointer",
                        width: "100px",
                        outline: "none",
                        fontSize: "24px",
                        bgcolor: "#303030",
                        borderRadius: "7px",
                        border: "none",
                        mx: "30px",
                        px: "12px",
                        py: "7px",
                        filter: "drop-shadow(4px 8px 6px rgba(0, 0, 0, 0.5))",
                        "& .css-11u53oe-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input":
                          {
                            p: 0,
                          },
                        "& .css-hfutr2-MuiSvgIcon-root-MuiSelect-icon": {
                          fontSize: "40px",
                          color: "text.secondary",
                        },
                      }}
                      value={lang}
                      onChange={this.handleChange}
                    >
                      {languages.map((el) => (
                        <MenuItem
                          selected={lang === el}
                          value={el}
                          key={el}
                          sx={{
                            color: "text.secondary",
                            bgcolor: "#303030",
                            "& .css-6hp17o-MuiList-root-MuiMenu-list": {
                              bgcolor: "#303030",
                              border: "1px solid #c4c2c2",
                            },
                          }}
                        >
                          {el}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                  <Box>
                    <Box
                      component="img"
                      src="/images/cart.png"
                      sx={{
                        width: "25px",
                        height: "25px",
                      }}
                    ></Box>
                    {nr_products > 0 ? (
                      <Box
                        component="sup"
                        sx={{
                          bgcolor: main[0].color,
                          display: "flex",
                          position: "absolute",
                          transform: "translate(15px, -42px)",
                          alignItems: "center",
                          justifyContent: "center",
                          fontSize: "13px",
                          width: "27px",
                          height: "27px",
                          borderRadius: "50%",
                          textAlign: "center",
                        }}
                      >
                        {nr_products}
                      </Box>
                    ) : (
                      ""
                    )}
                  </Box>
                </Box>
              </Box>
              <Box
                sx={{
                  mt: "60px",
                  width: "100%",
                  display: "flex",
                  flexDirection: this.state.items[0].isMainCompany
                    ? "row"
                    : "row-reverse",
                }}
              >
                {this.state.items.map((el) => (
                  <Box
                    sx={{
                      width: "50%",
                      display: "flex",
                      justifyContent: "space-between",
                      alignItems: "flex-end",
                      flexDirection: el.isMainCompany ? "row" : "row-reverse",
                      pr: el.isMainCompany ? "15px" : "0px",
                      pl: !el.isMainCompany ? "15px" : "0px",
                    }}
                  >
                    <Box
                      sx={{
                        width: "100%",
                      }}
                    >
                      {el.isMainCompany ? (
                        <Box
                          sx={{
                            color: "text.secondary",
                            fontSize: "34px",
                          }}
                        >
                          {el.slogan[i].content}
                        </Box>
                      ) : (
                        ""
                      )}
                      {el.isMainCompany ? (
                        <Box
                          sx={{
                            cursor: "pointer",
                            mt: "60px",
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            borderRadius: "12px",
                            p: "10px",
                            filter:
                              "drop-shadow(4px 8px 6px rgba(0, 0, 0, 0.5))",
                            border: "2px solid #c3c2c2",
                          }}
                        >
                          <Box
                            component="img"
                            src="/images/phone.png"
                            sx={{ pr: "10px" }}
                          ></Box>
                          <Box
                            component="span"
                            sx={{
                              width: "auto",
                              color: "#c4c2c2",
                              fontSize: "20px",
                            }}
                          >
                            {data[i].FAST_ORDER}
                          </Box>
                        </Box>
                      ) : (
                        ""
                      )}
                      <Link to={"/products/" + el.slug}>
                        <Box
                          sx={{
                            bgcolor: el.color,
                            cursor: "pointer",
                            mt: "40px",
                            py: "15px",
                            ml: "auto",
                            width: !el.isMainCompany
                              ? " calc(100% - 20px)"
                              : "100%",
                            filter:
                              "drop-shadow(4px 8px 6px rgba(0, 0, 0, 0.5))",
                            fontSize: "20px",
                            borderRadius: "12px",
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                          }}
                        >
                          {data[i].MENU + " " + el.name.toUpperCase()}
                        </Box>
                      </Link>
                      <Box
                        sx={{
                          mt: "30px",
                          display: "flex",
                          justifyContent: el.isMainCompany
                            ? "flex-start"
                            : "flex-end",
                        }}
                      >
                        <Box component="a" href={el.facebook}>
                          <Box
                            component="img"
                            src="/images/facebook.png"
                            sx={{
                              mx: !el.isMainCompany ? "20px" : "0px",
                            }}
                          ></Box>
                        </Box>
                        <Box component="a" href={el.instagram}>
                          <Box
                            component="img"
                            src="/images/instagram.png"
                            sx={{
                              mx: el.isMainCompany ? "20px" : "0px",
                            }}
                          ></Box>
                        </Box>
                      </Box>
                    </Box>
                    <Box
                      component="img"
                      src={this.url + el.mainProductImage.path}
                      sx={{
                        filter: "drop-shadow(0 16px 30px #000000)",
                        width: "15vw",
                        pl: el.isMainCompany ? "20px" : "0px",
                        margin: "auto",
                      }}
                    ></Box>
                  </Box>
                ))}
              </Box>

              <Box></Box>
            </Box>
          </ThemeProvider>
        </React.Fragment>
      );
    }
  }
}

export default Menu;
