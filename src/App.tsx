import React from "react";
import "./App.scss";
import Rout from "./landing/routes/Route";
import Navbar from "./landing/components/navbar/Navbar";
import { UIEvent } from "react";

interface AppProps {}

interface AppState {}

class App extends React.Component<AppProps, AppState> {
  constructor(props: AppProps) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <React.Fragment>
        <Rout />
      </React.Fragment>
    );
  }
}

export default App;
